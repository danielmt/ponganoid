.PHONY=dependencies

dependencies:
	npm install -g yo grunt-cli bower generator-webapp grunt-contrib
	bower install
